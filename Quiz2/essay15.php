Tulislah sintaks SQL untuk membuat tabel-tabel berikut:

table customers ( id integer auto increment primary key, name varchar(255), email varchar 255), password varchar(255);


table orders (id integer auto increment primary key, amount integer, customer_id integer foreign key references id on customers).

CREATE TABLE customers ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

CREATE TABLE orders (id int AUTO_INCREMENT PRIMARY KEY, amount int, customer_id int, FOREIGN KEY (customer_id) REFERENCES customers(id));