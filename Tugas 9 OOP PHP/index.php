<?php
    require_once 'Animals.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    $sheep = new Animal("shaun");
    $kodok = new Frog('buduk');
    $sungokong = new Ape('kera sakti');

    echo "Name : ". $sheep->name . "<br>"; // "shaun"
    echo "Leg : ".$sheep->legs. "<br>"; // 4
    echo "Cold Blooded : ".$sheep->cold_blooded. "<br> <br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : ". $kodok->name . "<br>"; // "shaun"
    echo "Leg : ".$kodok->legs. "<br>"; // 4
    echo "Cold Blooded : ".$kodok->cold_blooded. "<br>"; 
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name : ". $sungokong->name . "<br>"; // "shaun"
    echo "Leg : ".$sungokong->legs. "<br>"; // 4
    echo "Cold Blooded : ".$sungokong->cold_blooded. "<br>";
    $sungokong->yell() // "Auooo"

?>