<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\UlasanController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.halamanutama');
});
Route::get('/erd', function () {
    return view('pages.erd');
});

//Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/data-tables', function () {
        return view('pages.datatable');
    });
    Route::get('/table', [TableController::class, 'table']);
    // Route::get('/', function () {
    //     return view('layouts.master');
    // });
    
    //Routes Tugas 15
    Route::get('/cast/create', [CastController::class, 'create']);
    Route::get('/cast', [CastController::class, 'index']);
    Route::post('/cast', [CastController::class, 'store']);
    Route::get('/cast/{cast_id}', [CastController::class, 'show']);
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);
    Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

    //Routes Profile
    Route::resource('/profile', ProfileController::class)->only([
        'index', 
        // 'show', 'edit', 
        'update',
    ]);
    
});

//Routes Eloquen ORM
Route::resource('/post', PostController::class);

Auth::routes();

Route::resource('/film', FilmController::class);

Route::resource('/genre', GenreController::class);

Route::post('/ulasan/{film_id}', [UlasanController::class, 'store']);

