@extends('layouts.master')

@section('title')
    Tampilkan Detail Post
@endsection

@section('content')
<div class="card" style="width: 18rem;">
    <img src="{{asset('images/'.$post->thumbnail)}}" class="card-img-top" alt="...">
    <div class="card-body">
        <h2>Show Post {{$post->id}}</h2>
      <h5 class="card-title">{{$post->title}}</h5>
      <p class="card-text">{{$post->body}}</p>
      <a href="/post" class="btn btn-info">Kembali</a>
    </div>
</div>
{{-- <form action="/ulasan/{{$post->id}}" method="POST">
@csrf
  <div class="form-group">
      <label for="exampleFormControlTextarea1">Ulasan</label>
      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="ulasan"></textarea>
      @error('ulasan')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  </div>
  <button type="submit" value="ulasan" class="btn btn-primary">Submit</button>

</form> --}}

  {{-- <div class="card mb-10">
    <img src="{{asset('images/'.$post->thumbnail)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h2>Show Post {{$post->id}}</h2>
      <h5 class="card-title">{{$post->title}}</h5>
      <p class="card-text">{{$post->body}}</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      <a href="/post" class="btn btn-info">Kembali</a>
    </div>
  </div> --}}

  {{-- <img src="{{asset('images/'.$post->thumbnail)}}" alt="" width="100px">
    <h2>Show Post {{$post->id}}</h2>
    <h4>{{$post->title}}</h4>
    <p>{{$post->body}}</p>
    <a href="/post" class="btn btn-info">Kembali</a> --}}
@endsection