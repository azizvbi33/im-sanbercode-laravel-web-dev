
@extends('layouts.master')

@section('title')
    Data Post
@endsection

@section('content')
    <div>
        <h2>Edit Post {{$post->id}}</h2>
        <form action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" value="{{$post->title}}" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">body</label>
                <input type="text" class="form-control" name="body"  value="{{$post->body}}"  id="body" placeholder="Masukkan Body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">thumbnail</label>
                <input type="file" class="form-control" name="thumbnail" id="body" placeholder="Masukkan Body">
                @error('thumbnail')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">User Id</label>
                <select name="user_id" id="" class="form-control">
                    <option value="">-- Pilih User Id Kamu --</option>
                    @forelse ($user as $item)
                        @if ($item->id == $post->user_id)
                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                        @else 
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endif
                        @empty
                        <option value="">No Data</option>
                    @endforelse
                </select>
        
                @error('thumbnail')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection


