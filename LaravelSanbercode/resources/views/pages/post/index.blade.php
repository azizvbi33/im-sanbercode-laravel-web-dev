@extends('layouts.master')

@section('title')
    Data Post
@endsection

@section('content')
@auth
<a href="/post/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Thumbnail</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($post as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{{ Str::limit($value->body, 10)}}</td>
                        <td><img src="{{asset('images/'.$value->thumbnail)}}" alt="" width="100px"></td>
                        <td>
                            <a href="/post/{{$value->id}}" class="btn btn-info">Show</a>
                            
                            @auth
                            <a href="/post/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/post/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            @endauth

                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection