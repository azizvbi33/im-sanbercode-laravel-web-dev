@extends('layouts.master')

@section('title')
    Silahkan Buat Data Baru
@endsection

@section('content')
<h2>Tambah Data</h2>
<form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Body</label>
        <input type="text" class="form-control" name="body" id="body" placeholder="Masukkan Body">
        @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">thumbnail</label>
        <input type="file" class="form-control" name="thumbnail" id="body" placeholder="Masukkan Body">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">User Id</label>
        <select name="user_id" id="" class="form-control">
            <option value="">-- Pilih User Id Kamu --</option>
            @forelse ($user as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                @empty
                <option value="">No Data</option>
            @endforelse
        </select>

        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection