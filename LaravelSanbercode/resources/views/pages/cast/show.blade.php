@extends('layouts.master')

@section('title')
    Data Dari Database Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah</a>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nomor</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Detail</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{$key +1}}</th>
                    <td>{{ $item->nama }}</th>
                    <td>{{ $item->umur }}</td>
                    <td>
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    </td>
                    <td>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    </td>
                    <td>
                        <form action="/cast/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" value="delete" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <p>No users</p>
            @endforelse
          
        </tbody>
      </table>

@endsection