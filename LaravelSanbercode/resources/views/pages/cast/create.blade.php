@extends('layouts.master')

@section('title')
    Silahkan Buat Data Baru
@endsection

@section('content')
<form method="POST" action="/cast">
    @csrf
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Nama Kamu</label>
      <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label class="form-label">Umur</label>
      <input type="number" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label class="form-label">Biografi</label>
      <textarea rows="5" class="form-control" name="bio" ></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection