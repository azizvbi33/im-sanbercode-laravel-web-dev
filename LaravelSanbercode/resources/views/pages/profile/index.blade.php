@extends('layouts.master')

@section('title')
    Silahkan Buat Data Baru
@endsection

@section('content')
<form method="POST" action="/profile/{{$profile->id}}">
    @csrf
    @method('PUT')

    <div class="mb-3">
      <label class="form-label">Nama User</label>
      <input  value="{{$profile->user->name}}" class="form-control" disabled>
    </div>

    <div class="mb-3">
      <label class="form-label">Email User</label>
      <input value="{{$profile->user->email}}" class="form-control" disabled>
    </div>

    <div class="mb-3">
      <label class="form-label">Umur</label>
      <input type="number" value="{{$profile->umur}}" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label class="form-label">Biografi</label>
      <textarea rows="5" class="form-control" name="bio" > {{$profile->bio}} </textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Alamat</label>
        <input type="text" value="{{$profile->alamat}}" class="form-control" name="alamat">
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection