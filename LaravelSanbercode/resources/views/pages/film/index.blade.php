@extends('layouts.master')

@section('title')
    Data film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Ringkasan</th>
                <th scope="col">Nama Genre</th>
                <th scope="col">Poster</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($film as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{ Str::limit($value->ringkasan, 10)}}</td>
                        <td>{{$value->genre->nama}}</td>
                        <td><img src="{{asset('images/film/'.$value->poster)}}" alt="" width="100px"></td>
                        <td>
                            <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                            
                            @auth
                            <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/film/{{$value->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            @endauth

                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection