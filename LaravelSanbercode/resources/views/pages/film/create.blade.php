@extends('layouts.master')

@section('title')
    Silahkan Buat Data Baru
@endsection

@section('content')
<h2>Tambah Data</h2>
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label >judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">ringkasan</label>
        <input type="text" class="form-control" name="ringkasan" id="ringkasan" placeholder="Masukkan ringkasan">
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="ringkasan">Tahun</label>
        <input type="number" class="form-control" name="tahun" id="ringkasan" placeholder="Masukkan ringkasan">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="ringkasan">poster</label>
        <input type="file" class="form-control" name="poster" id="ringkasan" placeholder="Masukkan ringkasan">
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">User Id</label>
        <select name="genre_id" id="" class="form-control">
            <option value="">-- Pilih User Id Kamu --</option>
            @forelse ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                <option value="">No Data</option>
            @endforelse
        </select>

        @error('genre_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection