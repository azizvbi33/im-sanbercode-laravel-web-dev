@extends('layouts.master')

@section('title')
    Tampilkan Detail film
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-6">
      <div class="card">
        <img src="{{asset('images/film/'.$film->poster)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <span>{{$film->genre->nama}}</span>
            <h2>Show film {{$film->id}}</h2>
          <h5 class="card-title">{{$film->judul}}</h5>
          <p class="card-text">{{$film->ringkasan}}</p>
    
          <hr>
          <h3>List Komentar</h3>
          @forelse ($film->ulasan as $ulasan)
            <div class="card mb-3">
              <div class="card-body">
                <h5 class="card-title">{{$ulasan->user->name}}</h5>
                <p class="card-text">{{$ulasan->content}}</p>
              </div>
            </div>
          @empty
            <div class="alert alert-danger">
              Tidak ada ulasan
            </div>
          @endforelse
        </div>
        <a href="/film" class="btn btn-info">Kembali</a>
      </div>    
    </div>
    <div class="col-6"> 
  <div class="card">
    <div class="card-body">
      <h2>Tambahkan Ulasan</h2>
      <form action="/ulasan/{{$film->id}}" method="POST">
        @csrf
          <div class="form-group">
              <label for="exampleFormControlTextarea1">Ulasan</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="content"></textarea>
              @error('content')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
          <button type="submit" value="content" class="btn btn-primary">Submit</button>
        </form>
  </div>
  </div>
</div>



@endsection