
@extends('layouts.master')

@section('judul')
    Data film
@endsection

@section('content')
    <div>
        <h2>Edit film {{$film->id}}</h2>
        <form action="/film/{{$film->id}}" method="Post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" name="judul" value="{{$film->judul}}" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">ringkasan</label>
                <input type="text" class="form-control" name="ringkasan"  value="{{$film->ringkasan}}"  id="ringkasan" placeholder="Masukkan ringkasan">
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">Tahun</label>
                <input type="number" class="form-control" name="tahun" value="{{$film->tahun}}" id="ringkasan" placeholder="Masukkan ringkasan">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">poster</label>
                <input type="file" class="form-control" name="poster" value="{{asset('images/film/'.$film->poster)}}" id="ringkasan" placeholder="Masukkan ringkasan">
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">genre Id</label>
                <select name="genre_id" id="" class="form-control">
                    <option value="">-- Pilih genre Id Kamu --</option>
                    @forelse ($genre as $item)
                        @if ($item->id == $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else 
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                        @empty
                        <option value="">No Data</option>
                    @endforelse
                </select>
        
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection


