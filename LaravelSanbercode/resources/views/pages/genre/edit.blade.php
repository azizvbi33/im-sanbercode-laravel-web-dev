
@extends('layouts.master')

@section('nama')
    Data film
@endsection

@section('content')
    <div>
        <h2>Edit film {{$film->id}}</h2>
        <form action="/film/{{$film->id}}" method="Post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" value="{{$film->nama}}" id="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection


