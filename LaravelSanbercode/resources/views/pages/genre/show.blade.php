@extends('layouts.master')

@section('title')
    Tampilkan Detail film
@endsection

@section('content')
<h1>{{$genre->nama}}</h1>

<div class="row">
  @forelse ($genre->film as $item)
    <div class="col-4">
      <div class="card" style="width: 18rem;">
        <img src="{{asset('images/film/'.$item->poster)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <span>{{$item->genre->nama}}</span>
            <h2>Show film {{$item->id}}</h2>
          <h5 class="card-title">{{$item->judul}}</h5>
          <p class="card-text">{{$item->ringkasan}}</p>
          <a href="/film/{{$item->id}}" class="btn btn-info">Readme</a>
        </div>
      </div>
    </div>
    @empty
        Tidak ada film di genre ini
    @endforelse
</div>
  {{-- <div class="card mb-10">
    <img src="{{asset('images/'.$film->poster)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h2>Show film {{$film->id}}</h2>
      <h5 class="card-judul">{{$film->judul}}</h5>
      <p class="card-text">{{$film->body}}</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      <a href="/film" class="btn btn-info">Kembali</a>
    </div>
  </div> --}}

  {{-- <img src="{{asset('images/'.$film->poster)}}" alt="" width="100px">
    <h2>Show film {{$film->id}}</h2>
    <h4>{{$film->judul}}</h4>
    <p>{{$film->body}}</p>
    <a href="/film" class="btn btn-info">Kembali</a> --}}
@endsection