<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('/template/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">

        @auth
        <a href="#" class="d-block">{{ Auth::user()->name }} ({{Auth::user()->profile->umur}})</a>
        <a href="">{{Auth::user()->profile->alamat}}</a>
        @endauth

        @guest
        <a href="#" class="d-block">Anda Belum Login</a>
        @endguest

      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/data-tables" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Dashboard Utama
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
        </li>
        <li class="nav-item">
          <a href="/erd" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              ERD
              <span class="right badge badge-info">New</span>
            </p>
          </a>
      </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Tables
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tables</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/data-tables" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Tables Data</p>
              </a>
            </li>
          </ul>
        </li> 
        <li class="nav-item">
          <a href="/post" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Post
              <span class="right badge badge-warning">New</span>
            </p>
          </a>
        </li>
        @auth
        <li class="nav-item">
          <a href="/post/create" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Tambah Data Post
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li> 
        @endauth 
        <li class="nav-item">
          <a href="/film/create" class="nav-link">
            <i class="nav-icon fas fa-film"></i>
            <p>
              Tambah Data Film
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li> 
        <li class="nav-item">
          <a href="/film" class="nav-link">
            <i class="nav-icon fas fa-film"></i>
            <p>
              Data Film
              <span class="right badge badge-primary">New</span>
            </p>
          </a>
        </li> 
        <li class="nav-item">
          <a href="/genre" class="nav-link">
            <i class="nav-icon fas fa-film"></i>
            <p>
              Data Genre
              <span class="right badge badge-primary">New</span>
            </p>
          </a>
        </li> 
        @auth
        <li class="nav-item">
          <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
              Edit Profile
            </p>
          </a>
        </li> 
        @endauth  

        @guest
        <li class="nav-item bg-info">
          <a href="/login" class="nav-link">
            <p>
              Login
            </p>
          </a>  
        @endguest

        @auth
        <li class="nav-item bg-danger" aria-labelledby="navbarDropdown">
          <a class="nav-link" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
        </li>
        @endauth
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>