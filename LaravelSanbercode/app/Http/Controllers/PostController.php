<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use File;

class PostController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('log')->only('index');
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return view('pages.post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();
        return view('pages.post.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'title' => 'required',
    		'body' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'user_id' => 'required'
    	]);

        $thumbnail = $request->file('thumbnail');
        $new_thumbnail = time().$thumbnail->getClientOriginalName();
        $thumbnail->move(public_path('images'), $new_thumbnail);
 
        Post::create([
    		'title' => $request->title,
    		'body' => $request->body,
            'thumbnail' => $new_thumbnail,
            'user_id' => $request->user_id,
    	]);
 
    	return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('pages.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $user = User::get();

        return view('pages.post.edit', ['post' => $post, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
    		'body' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'user_id' => 'required'
        ]);

        $post = post::find($id);

        if($request->hasFile('thumbnail')){
            $destination = 'images/'.$post->thumbnail;
            if(File::exists($destination)){
                File::delete($destination);
            }
            $thumbnail = $request->file('thumbnail');
            $new_thumbnail = time().$thumbnail->getClientOriginalName();
            $thumbnail->move(public_path('images'), $new_thumbnail);
            $post->thumbnail = $new_thumbnail;
        }

        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = $request->user_id;
        $post->update();
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function destroy($id)
        {
            $post = Post::find($id);
            $path = 'images/';
            File::delete($path.$post->thumbnail);
            $post->delete();
            return redirect('/post');
        }
}
