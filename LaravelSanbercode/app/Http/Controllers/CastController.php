<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('pages.cast.create');
    }

    public function store(Request $request){
        //Validasi Data From
        $request->validate([
            'nama' => 'required|min:5|max:255',
            'umur' => 'required|numeric',
            'bio' => 'required|min:20|max:255',
        ]);

        //Insert Data ke Table Cast
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        //Redirect ke halaman cast
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
       return view('pages.cast.show', ['cast' => $cast]);
    }

    public function show($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('pages.cast.detail', ['cast' => $cast]);
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('pages.cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $cast_id){
        //Validasi Data From
        $request->validate([
            'nama' => 'required|min:5|max:255',
            'umur' => 'required|numeric',
            'bio' => 'required|min:20|max:255',
        ]);

        //Update Data ke Table Cast
        DB::table('cast')->where('id', $cast_id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        //Redirect ke halaman cast
        return redirect('/cast');
    }

    public function destroy($cast_id){
        DB::table('cast')->where('id', $cast_id)->delete();

        return redirect('/cast');
    }

}
