<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
 
class ProfileController extends Controller
{
    public function index()
    {
        
        // Retrieve the currently authenticated user's ID...
        $idsuer = Auth::id();
        $profile = Profile::where('user_id', $idsuer)->first();
        return view('pages.profile.index', ['profile' => $profile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required|numeric',
            'bio' => 'required|min:5|max:255',
            'alamat' => 'required|min:5|max:255',
        ]);
        $profile = Profile::find($id);
 
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;
        
        $profile->save();

        return redirect('/post');
    }
    // public function index()
    // {
    //     $profile = auth()->user()->profile;
    //     return view('pages.profile', compact('profile'));
    // }

    // public function update(Request $request)
    // {
    //     $profile = auth()->user()->profile;
    //     $profile->update($request->all());
    //     return redirect()->back();
    // }
}
