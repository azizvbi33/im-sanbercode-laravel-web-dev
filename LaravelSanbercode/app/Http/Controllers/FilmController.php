<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use Illuminate\Support\Facades\DB;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('pages.film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('pages.film.create', ['genre' => $genre]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'genre_id' => 'required'
    	]);

        $poster = $request->file('poster');
        $new_poster = time().$poster->getClientOriginalName();
        $poster->move(public_path('images/film/'), $new_poster);
 
        Film::create([
    		'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,                    
            'poster' => $new_poster,
            'genre_id' => $request->genre_id,
    	]);
 
    	return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('pages.film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::get();

        return view('pages.film.edit', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'genre_id' => 'required'
        ]);

        $film = Film::find($id);

        if($request->hasFile('poster')){
            $destination = 'images/film/'.$film->poster;
            if(File::exists($destination)){
                File::delete($destination);
            }
            $poster = $request->file('poster');
            $new_poster = time().$poster->getClientOriginalName();
            $poster->move(public_path('images/film/'), $new_poster);
            $film->poster = $new_poster;
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->update();
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function destroy($id)
        {
            $film = Film::find($id);
            $path = 'images/film/';
            File::delete($path.$film->poster);
            $film->delete();
            return redirect('/film');
        }
}
