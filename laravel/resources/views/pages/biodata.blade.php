<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Halaman Biodata</h1>
    <form action="/welcome" method="post">
        @csrf
        <label for="nama">Nama Depan</label>
        <br>
        <input type="text" name="nama_depan" id="nama">
        <br>
        <label for="nama">Nama Belakang</label>
        <br>
        <input type="text" name="nama_belakang" id="nama">
        <br>
        <label for="email">Email</label>
        <br>
        <input type="email" name="email" id="email">
        <br>
        <label for="alamat">Alamat</label>
        <br>
        <input type="text" name="alamat" id="alamat">
        <br>
        <label for="jenis_kelamin">Jenis Kelamin</label>
        <br>
        <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Laki-laki">Laki-laki
        <br>
        <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan">Perempuan
        <br>
        <label for="agama">Agama</label>
        <br>
        <select name="agama" id="agama">
            <option value="Islam">Islam</option>
            <option value="Kristen">Kristen</option>
            <option value="Hindu">Hindu</option>
            <option value="Budha">Budha</option>
            <option value="Konghucu">Konghucu</option>
        </select>
        <br>
        <br>
        <input type="submit" value="kirim">
    </form>
    
</body>
</html>