<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BioController extends Controller
{
    public function daftar()
    {
        return view('pages.biodata');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['nama_depan'];
        $namaBelakang = $request['nama_belakang'];
        $email = $request['email'];
        $jenisKelamin = $request['jenis_kelamin'];

        return view('pages.welcome', 
        [
            'namaDepan' => $namaDepan,
            'namaBelakang' => $namaBelakang,
            'email' => $email,
            'jenisKelamin' => $jenisKelamin]);
    }

}
