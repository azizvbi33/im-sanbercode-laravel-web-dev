// Tipe Data
var kalimat = "Ini adalah kalimat"; // String
var angka = 10; // Number
var angkaDesimal = 10.5; // Number
var boolean = true; // Boolean
var array = ["aziz", "evie", "david", "daniel"]; // Array memanggil Index [index] = value
var object = {nama: "aziz", umur: 20, pekerjaan: "mahasiswa"}; // Object Mengganti Index degan nam a= {key: value, key: value, key: value} {JSON}

// String Method
var kalimat = "Ini adalah kalimat";
subtring(); // mengambil string dari index ke berapa sampai index ke berapa
//index pertama dimulai dari 0 dan index kedua dimulai dari 1
var res = kalimat.substring(0, 10);
console.log(res);
//output : Ini adalah

substr(); // mengambil string dari index ke berapa sampai index ke berapa
//index pertama dimulai dari 0 dan index kedua dimulai dari index yang diambil sampai panjang string yang diambil lagi
var su = kalimat.substr(11, 7);
console.log(su);
//output : kalimat




// split() // memecah string menjadi array
// toUpperCase() // mengubah string menjadi huruf besar
// toLowerCase() // mengubah string menjadi huruf kecil
// trim() // menghapus spasi di awal dan akhir string
// replace() // mengganti string dengan string lain
// indexOf() // mencari index dari string
// lastIndexOf() // mencari index dari string dari belakang
// charAt() // mencari karakter dari string
// concat() // menggabungkan string
// includes() // mencari string di dalam string
// startsWith() // mencari string di awal string
// endsWith() // mencari string di akhir string
// repeat() // mengulang string




// function munculkanAngkaDua() {
//     return 2
//   }
   
//   var tampung = munculkanAngkaDua();
//   console.log(tampung)
// function tampilkanAngka(angkaPertama, angkaKedua) {
//   return angkaPertama + angkaKedua
// }
// console.log(tampilkanAngka(5, 3));
